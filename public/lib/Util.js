'use strict';

export default class Util {

  /**
   * Load a script into the page, and execute the given callback on load completion.
   * @param src The path to the source file to be loaded.
   * @param cb The callback to be called on load completion.
   */
  static addScript(src, cb) {
    let s = document.createElement('script');
    s.setAttribute('src', src);
    s.onload = cb;
    document.body.appendChild(s);
  }
}
