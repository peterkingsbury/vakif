'use strict';

import PubSub from 'pubsub-js';
import Util from './Util';
import Promise from 'bluebird';

const WS_PROTOCOL = 'ws';
const WS_HOSTNAME = '127.0.0.1';
const WS_PORT = 8181; // This should match config/servers/web.js|default.port

class Net {
  constructor(game) {
    console.debug(`${this.constructor.name}::ctor()`);
    this.game = game;
    this._promise = this._initScript();
  }

  _initScript() {
    console.log(`Installing AH client script...`);
    return new Promise((resolve, reject) => {
      Util.addScript('/lib/ActionheroClient.js', result => {
        console.log(`Script installed.`);
        // Allocate a new AH client
        this.client = new window.ActionheroClient();

        // Bind its events to handlers in this class
        this.client.on('connected',    this.onConnected.bind(this));
        this.client.on('disconnected', this.onDisconnected.bind(this));
        this.client.on('error',        this.onError.bind(this));
        this.client.on('reconnect',    this.onReconnect.bind(this));
        this.client.on('reconnecting', this.onReconnecting.bind(this));
        this.client.on('message',      this.onMessage.bind(this));
        this.client.on('alert',        this.onAlert.bind(this));
        this.client.on('api',          this.onApi.bind(this));
        this.client.on('welcome',      this.onWelcome.bind(this));
        this.client.on('say',          this.onSay.bind(this));

        resolve();
      });
    });
  }

  connect() {
    this._promise = this._promise.then(() => {
      return new Promise((resolve, reject) => {
        console.info(`Connecting to ${WS_PROTOCOL}://${WS_HOSTNAME}:${WS_PORT} ...`);
        this.client.connect((err, details) => {
          if (err) {
            reject(err);
          } else {
            this.client.roomAdd('defaultRoom');
            resolve(details);
          }
        });
      });
    });

    return this._promise;
  }

  onConnected() {
    console.debug('Net::onConnected()');
    PubSub.publish('/net/connect');
  }

  onDisconnected() {
    console.debug('Net::onDisconnected()');
  }

  onError(err) {
    console.debug('Net::onError(): ' + err.toString());
  }

  onReconnect() {
    console.debug('Net::onReconnect()');
  }

  onReconnecting() {
    console.debug('Net::onReconnecting()');
  }

  onMessage(msg) {
    //console.debug('Net::onMessage()');
    PubSub.publish('/net/message', msg);
  }

  onAlert(msg) {
    console.debug('Net::onAlert()');
    PubSub.publish('/net/alert', msg);
  }

  onApi(msg) {
    console.debug('Net::onApi()');
    PubSub.publish('/net/api', msg);
  }

  onWelcome(msg) {
    console.debug('Net::onWelcome()');
    PubSub.publish('/net/welcome', msg);
  }

  onSay(msg) {
    //console.debug('Net::onSay()');
    PubSub.publish('/net/say', msg);
  }

}

export default Net;
