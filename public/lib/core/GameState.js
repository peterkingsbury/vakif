'use strict';

import Machina from 'machina';

class GameState {

  constructor() {
    console.debug(`${this.constructor.name}::ctor()`);
  }

  _onEnter() {
    console.debug(`${this.constructor.name}::_onEnter()`);
  }

  _onExit() {
    console.debug(`${this.constructor.name}::_onExit()`);
    this.handle('cleanup');
  }

  cleanup() {
    console.debug(`${this.constructor.name}::cleanup()`);
  }

  '*'() {

  }
}

export default GameState;
