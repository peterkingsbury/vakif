'use strict';

import GameState from '../core/GameState';
import Net from '../Net';

class Boot extends GameState {

  _onEnter() {
    console.debug(`Boot::_onEnter()`);
    this.handle('_buildApplication');
  }

  _onExit() {
    console.debug('Boot::_onExit()');
  }

  _buildApplication() {
    console.log('Building the application...');

    this.game.net = new Net(this);
    this.game.net.connect().then(details => {
      this.handle('_onApplicationBuilt');
    });
  }

  _onApplicationBuilt() {
  }

  onKeyDown(e) {
    //console.log(e);
  }

  onKeyUp(e) {
    //console.log(e);
  }

  onKeyPress(e) {
    console.log(e);
  }

  cleanup() {
    //console.log('Boot::cleanup()');
  }
}

export default Boot;
