'use strict';

import PubSub from 'pubsub-js';   // https://github.com/mroderick/PubSubJS
import Machina from 'machina';    // http://machina-js.org/
import Boot from './state/Boot';

class Game {
  constructor() {
    let game = this;

    // Instantiate our game FSM.
    this.state = new Machina.Fsm({
      initialize:   options => {
      },
      namespace:    'Root',
      initialState: 'Uninitialized',
      states:       {
        Uninitialized: {},
        Boot:       new Boot()
      }
    });

    // Delay setting state objects until after constructors are complete
    this.state.game = this;

    // Set the Boot state to begin things.
    this.state.transition('Boot');
  }
}

export default Game;
