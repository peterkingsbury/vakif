const path = require('path');
const webpack = require('webpack');
const WS_PORT = 8181; // This should match config/servers/web.js|default.port

module.exports = {
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    inline: true,
    proxy: [
      {
        context: '/primus',
        target: 'http://127.0.0.1:' + WS_PORT,
        ws: true,
        secure: false
      }
    ]
  },
  entry: [
    'webpack-dev-server/client?http://localhost:8080/',
    './public/main.js'
  ],
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js',
    hashFunction: 'sha256'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        include: [path.resolve(__dirname, 'public')]
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components|public\/vendor)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015']
        }
      },
      { test: /\.svg$/, loader: 'file-loader?limit=65000&mimetype=image/svg+xml&name=[name].[ext]' },
      { test: /\.woff$/, loader: 'file-loader?limit=65000&mimetype=application/font-woff&name=[name].[ext]' },
      { test: /\.woff2$/, loader: 'file-loader?limit=65000&mimetype=application/font-woff2&name=[name].[ext]' },
      { test: /\.[ot]tf$/, loader: 'file-loader?limit=65000&mimetype=application/octet-stream&name=[name].[ext]' },
      { test: /\.eot$/, loader: 'file-loader?limit=65000&mimetype=application/vnd.ms-fontobject&name=[name].[ext]' },
      { test: /\.(eot|woff|woff2|svg|ttf)([\?]?.*)$/, loader: 'file-loader' }
    ]
  },
  devtool: '#inline-source-map'
};
