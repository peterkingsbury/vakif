# Vakif - The Foundation

## Installation

`npm install`

## To Run

### Client

`npm run ui-server`

### Server

`npm start`

## Notes

* pubsub-js - Broadcast messages between libraries, alleviating the need to share references.
* machina   - A finite state machine.
* babylonjs - A 3D WebGL rendering library.
* phaser    - Ultra-fast 2D rendering library based on Pixi.js
